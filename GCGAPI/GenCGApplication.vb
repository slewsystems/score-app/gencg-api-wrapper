﻿Imports System.IO
Imports System.Runtime.InteropServices
Imports com.slewsystems.gencg.api.WindowsApiUtility



Public Class GenCGApplication
    Public Event Updated(Info As GenCGApplicationInfo)

    Public Info As New GenCGApplicationInfo
    Private backhandle As IntPtr 'handle of window to focus back to when silent updating

    ''' <param name="keepfocushandle">this is used when calling SilentUpdate() in order to not lose focus of your application</param>
    Public Sub New(ByVal keepfocushandle As IntPtr)
        Me.New()
        backhandle = keepfocushandle
    End Sub

    Public Sub New()
        Me.Reset()
    End Sub

    Public Sub Reset()
        Info.Reset()
        backhandle = IntPtr.Zero
    End Sub

    ''' <summary>
    ''' Rescan all windows and processes to gather GenCG information, will raise update event
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub Update()
        Me.Reset()
        Info.Update()

        RaiseEvent Updated(Info)
    End Sub

    Public Overrides Function Equals(obj As Object) As Boolean
        If obj.GetType Is Me.GetType Then
            Return DirectCast(obj, GenCGApplication).Info.PID = Info.PID
        Else
            Return False
        End If
    End Function

    Public Function GetOpenedDocuments() As GenCGDocumentList
        Dim docs As New GenCGDocumentList
        If Info.MDIHandle <> IntPtr.Zero Then
            Dim lngHWnd As IntPtr = FindWindowEx(Info.MDIHandle, IntPtr.Zero, Nothing, Nothing)
            Do Until lngHWnd = IntPtr.Zero
                If lngHWnd <> IntPtr.Zero Then
                    docs.Add(New GenCGDocument(lngHWnd, Info.MDIHandle))
                End If
                lngHWnd = FindWindowEx(Info.MDIHandle, lngHWnd, Nothing, Nothing)
            Loop
        End If
        Return docs
    End Function

    Public Sub CycleActiveDocument()
        PostMessage(Info.MDIHandle, WINDOWS_MESSAGES.WM_MDINEXT, IntPtr.Zero, IntPtr.Zero)
    End Sub

    Public ReadOnly Property Running As Boolean
        Get
            Try
                Process.GetProcessById(Info.PID)
            Catch ex As Exception
                Return False
            End Try
            Return True
        End Get
    End Property

    Public Function OpenFile(filePath As String) As Boolean
        If System.IO.File.Exists(filePath) Then
            Dim psi = New ProcessStartInfo() With {.FileName = filePath, .UseShellExecute = True}
            Process.Start(psi)
            Return True
        End If

        Return False
    End Function

#Region "Window Managment"

    Public Sub Launch()
        System.Diagnostics.Process.Start(GenCGApplicationInfo.GetApplicationPath())
    End Sub

    Public Sub BringToFront()
        WindowsApiUtility.BringToFront(Info.WindowHandle)
    End Sub

    Public Sub Close()
        CloseHWnd(Info.WindowHandle)
    End Sub

    Public Sub Maximize()
        MaximizeHWnd(Info.WindowHandle)
    End Sub

    Public Sub SilentMaximize()
        WindowsApiUtility.SilentMaximize(Info.WindowHandle, backhandle)
    End Sub

    Public Sub SilentUpdate()
        WindowsApiUtility.SilentUpdate(Info.WindowHandle, backhandle)
    End Sub

#End Region

End Class