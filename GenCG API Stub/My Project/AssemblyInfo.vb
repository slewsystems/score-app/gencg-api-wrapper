﻿Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices

' General Information about an assembly is controlled through the following 
' set of attributes. Change these attribute values to modify the information
' associated with an assembly.

' Review the values of the assembly attributes

<Assembly: AssemblyTitle("GenCG CLI Utility")>
<Assembly: AssemblyDescription("")> 
<Assembly: AssemblyCompany("")>
<Assembly: AssemblyProduct("GenCG CLI")>
<Assembly: AssemblyCopyright("Copyright Slew Systems LLC 2017")>
<Assembly: AssemblyTrademark("Slew Systems LLC")>

<Assembly: ComVisible(False)>

'The following GUID is for the ID of the typelib if this project is exposed to COM
<Assembly: Guid("67e4072a-f436-4552-afd1-df267ca49db9")> 

' Version information for an assembly consists of the following four values:
'
'      Major Version
'      Minor Version 
'      Build Number
'      Revision
'
' You can specify all the values or you can default the Build and Revision Numbers 
' by using the '*' as shown below:
' <Assembly: AssemblyVersion("1.0.*")> 

<Assembly: AssemblyVersion("1.0.0.0")> 
<Assembly: AssemblyFileVersion("1.0.0.0")> 
