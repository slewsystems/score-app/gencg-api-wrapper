﻿Imports System.Runtime.InteropServices

Module WindowsApiUtility

#Region "Win32 API Functions"

    Declare Auto Function SetWindowText Lib "user32.dll" (ByVal hwnd As IntPtr, ByVal lpString As String) As Boolean
    Declare Function ShowWindow Lib "user32.dll" (ByVal hWnd As IntPtr, ByVal nCmdShow As SHOW_WINDOW) As Boolean
    Declare Function SetForegroundWindow Lib "user32.dll" (ByVal hWnd As IntPtr) As Long
    <DllImport("user32.dll", CharSet:=CharSet.Auto, ExactSpelling:=True)>
    Function GetDesktopWindow() As IntPtr
    End Function
    <DllImport("user32.dll", SetLastError:=True)>
    Function GetWindowThreadProcessId(ByVal hwnd As IntPtr, ByRef lpdwProcessId As Integer) As Integer
    End Function
    <DllImport("user32.dll", SetLastError:=True, CharSet:=CharSet.Auto)>
    Function FindWindowEx(ByVal parentHandle As IntPtr, ByVal childAfter As IntPtr, ByVal lclassName As String, ByVal windowTitle As String) As IntPtr
    End Function
    <DllImport("user32.dll", SetLastError:=True, CharSet:=CharSet.Auto)>
    Function PostMessage(ByVal hWnd As IntPtr, ByVal Msg As UInteger, ByVal wParam As IntPtr, ByVal lParam As IntPtr) As Boolean
    End Function
    <DllImport("user32.dll", SetLastError:=True, CharSet:=CharSet.Auto)>
    Function SendMessage(ByVal hWnd As IntPtr, ByVal Msg As UInteger, ByVal wParam As IntPtr, ByVal lParam As IntPtr) As IntPtr
    End Function
    Declare Function GetWindowTextLength Lib "user32.dll" Alias "GetWindowTextLengthA" (ByVal hwnd As IntPtr) As Int32
    Declare Function GetWindowText Lib "user32.dll" Alias "GetWindowTextA" (ByVal hwnd As IntPtr, ByVal lpString As String, ByVal cch As Int32) As Int32
    Declare Auto Function IsIconic Lib "user32.dll" (ByVal hwnd As IntPtr) As Boolean
    <DllImport("user32.dll", CharSet:=CharSet.Auto)>
    Function IsWindowVisible(ByVal hWnd As IntPtr) As Boolean
    End Function

    Public Enum SHOW_WINDOW As Integer
        SW_HIDE = 0
        SW_NORMAL = 1
        SW_SHOWMINIMIZED = 2
        SW_MAXIMIZE = 3
        SW_SHOWNOACTIVATE = 4
        SW_SHOW = 5
        SW_MINIMIZE = 6
        SW_SHOWMINNOACTIVE = 7
        SW_SHOWNA = 8
        SW_RESTORE = 9
        SW_SHOWDEFAULT = 10
        SW_FORCEMINIMIZE = 11
        SW_MAX = 11
    End Enum

    Public Enum SYSTEM_COMMAND As Long
        SC_CLOSE = &HF060
        SC_MAXIMIZE = &HF030
        SC_MINIMIZE = &HF020
        SC_RESTORE = &HF120
    End Enum

    Public Enum WINDOWS_MANAGMENT As Integer
        ACTIVATE = 1
        DEACTIVATE = 0
    End Enum

    Public Enum WINDOWS_MESSAGES As UInteger
        WM_ACTIVATE = &H6
        WM_MDIACTIVATE = &H222
        WM_MDIGETACTIVE = &H229
        WM_MDIMAXIMIZE = &H225
        WM_MDINEXT = &H224
        WM_MDIRESTORE = &H223
        WM_MDITILE = &H226
        WM_SYSCOMMAND = &H112
        WM_NCPAINT = &H85
    End Enum

#End Region

    Function GetWindowTitleFromHandle(handle As IntPtr) As String
        Dim str As String = Space(GetWindowTextLength(handle) + 1)
        If GetWindowText(handle, str, str.Length) > 0 Then
            Return str.Substring(0, str.Length - 1).Trim
        End If
        Return String.Empty
    End Function

    Function GetPIDFromHandle(handle As IntPtr) As Integer
        If handle <> IntPtr.Zero Then
            Dim pid As Integer
            GetWindowThreadProcessId(handle, pid)
            Return pid
        End If
        Return -1
    End Function

    Function GetPIDFromProcess(processName As String) As Integer
        Return Process.GetProcessesByName(processName)(0).Id
    End Function

    Sub BringToFront(handle As IntPtr)
        If IsIconic(handle) Then 'if minimized
            MaximizeHWnd(handle)
        End If
        SetForegroundWindow(handle)
    End Sub

    Sub ManageWindow(handle As IntPtr, action As SYSTEM_COMMAND)
        PostMessage(handle, CUInt(WINDOWS_MESSAGES.WM_SYSCOMMAND), CType(action, IntPtr), IntPtr.Zero)
    End Sub

    Sub CloseHWnd(handle As IntPtr)
        ManageWindow(handle, SYSTEM_COMMAND.SC_CLOSE)
    End Sub

    Sub MaximizeHWnd(handle As IntPtr)
        ManageWindow(handle, SYSTEM_COMMAND.SC_MAXIMIZE)
    End Sub

    Sub SilentUpdate(focushandle As IntPtr, backtohandle As IntPtr)
        If IsIconic(focushandle) Then 'if minimized
            MaximizeHWnd(focushandle)
        End If
        PostMessage(focushandle, WINDOWS_MESSAGES.WM_ACTIVATE, CType(1, IntPtr), IntPtr.Zero)
        ShowWindow(backtohandle, SHOW_WINDOW.SW_SHOW)
    End Sub

    Sub SilentMaximize(focushandle As IntPtr, backtohandle As IntPtr)
        MaximizeHWnd(focushandle)
        ShowWindow(backtohandle, SHOW_WINDOW.SW_SHOW)
        BringToFront(backtohandle)
    End Sub

    Function FindWindowHandle(processName As String) As IntPtr
        Dim pHwnd As IntPtr = GetDesktopWindow()
        Dim lngHWnd As IntPtr = FindWindowEx(pHwnd, IntPtr.Zero, Nothing, Nothing)
        Do Until lngHWnd = IntPtr.Zero
            Dim title As String = GetWindowTitleFromHandle(lngHWnd)
            If Not String.IsNullOrEmpty(title.Trim) And title.Trim.ToLower.Contains(processName) Then 'find window with proctitle in title
                Dim pid As Integer
                GetWindowThreadProcessId(lngHWnd, pid) 'get pid of this window
                If System.Diagnostics.Process.GetProcessById(pid).ProcessName.ToLower = processName Then 'compare pid window exe to known procexe
                    Return lngHWnd
                End If
            End If
            lngHWnd = FindWindowEx(pHwnd, lngHWnd, Nothing, Nothing)
        Loop
        Return IntPtr.Zero
    End Function

    Function GetPathToProcessExecutable(pid As Integer) As String
        If pid > 0 Then
            Return System.Diagnostics.Process.GetProcessById(pid).MainModule.FileName
        End If
        Return String.Empty
    End Function

End Module
