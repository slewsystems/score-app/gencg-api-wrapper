# Changelog

## 0.1.0 (2/19/2017)
- Initial release!
- GenCG Windows API feature set
	- Launch GenCG
	- Open GenCG File
	- Switch between active documents in GenCG
	- Window management (maximize, close, minimize, bring to front)
	- MDI management (activate, maximize, close)
- GenCG TCP feature set
	- Cut
	- Clear
	- Take in
	- Take out
	- Play sequence
	- Stop sequence
	- Load page
	- Get document info (total pages, starting and ending page)