# Compix GenCG API

Custom DLL to provide applications direct interaction with GenCG processes and TCP API calls. This project includes a DLL as well as a simple CLI utility too.

## Enabling TCP/IP Support in GenCG
You can find more information in their [User Manual](http://www.compixstore.com/compixtv/pdf/GenCG%205.5%201.1.pdf). The enable TCP/IP do the following:
1. Open GenCG then Tools > Options
2. Go to the  External Controllers tab
3. In the APC section select a port (it doesn't matter which you use)
4. Check Auto Start
5. Click OK then close GenCG
6. Open GenCG then Tools > APC Client Mode
7. Select TCP/IP
8. Click START and close the window

---

## DLL Usage

### GenCGApplication Usage
```vb
Dim App As New GenCGApplication()
App.Update() ' Search and find GenCG instance

Dim Documents As GenCGDocumentList = App.GetOpenedDocuments()
Documents(1).Activate() ' Open the second opened document

App.OpenFile("C:\path\yourfile.gcg") ' Open an existing GenCG file
App.BringToFront() ' Focus the GenCG main window
```

### GenCGTCP Usage
```vb
Dim App as New GenCGTCP()
App.Connect() ' Connect to GenCG on its default TCP port

App.Cut(1) ' Cut the first page of active document
App.TakeIn(1, 2) ' TakeIn the first page in 2 seconds
```

---

## CLI Usage
The CLI utlity has a full help `--help` flag that will list all actions you can perform and for each action will list the supported flags and their descriptions too.

### Example Usage
- Cut the first page of the active document: `GenCGCLI.exe cut -p 1`
- Clear the current graphic: `GenCGCLI.exe clear`
- Start GenCG or focus an existing instance running: `GenCGCLI.exe launch`
- Open a specific GenCG file: `GenCGCLI.exe activate -f C:\path\yourfile.gcg`
- Find all documents that are opend: `GenCGCLI.exe workspace-info`