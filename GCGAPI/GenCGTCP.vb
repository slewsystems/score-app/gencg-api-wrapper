﻿Imports System.Linq
Imports System.Net.Sockets
Imports System.Text

Public Class GenCGTCP

    Public Enum COMMAND As Byte
        SEPARATOR = &HFF

        DOCUMENT_INFO = &H0
        START_SEQUENCE = &H10
        STOP_SEQUENCE = &H11
        LOAD_PAGE = &H20
        TAKE_IN = &H30
        TAKE_OUT = &H31
        CUT = &H32
        CLEAR = &H33
    End Enum

    Public Enum RESPONSE As Byte
        SEND_ERROR
        SUCCESS
        BAD_COMMAND = &H71
        BAD_PARAMETER = &H72
        CANNOT_PROCESS = &H73
        FINISH_PLAYBACK = &H74
    End Enum

    Public Structure DocumentInfo
        Public TotalPages As Int16
        Public StartPage As Int16
        Public EndPage As Int16
    End Structure

    Private netClient As TcpClient
    Private netStream As NetworkStream

    Public IP As String = "127.0.0.1"
    Public Port As Integer = 61225

    Public Function Connect() As Boolean
        Me.Disconnect()
        netClient = New TcpClient() With {
            .ReceiveTimeout = 500,
            .SendTimeout = 500,
            .ReceiveBufferSize = 16
        }

        Try
            netClient.Connect(IP, Port)
            netStream = netClient.GetStream()
        Catch ex As Exception
            Me.Disconnect()
        End Try

        Return Me.Connected
    End Function

    Public Sub Disconnect()
        If netClient IsNot Nothing Then
            netClient.Close()
            netClient = Nothing
        End If
        If netStream IsNot Nothing Then
            netStream.Close()
            netStream = Nothing
        End If
    End Sub

    Public ReadOnly Property Connected() As Boolean
        Get
            If netClient IsNot Nothing Then
                Return netClient.Connected
            End If
            Return False
        End Get
    End Property

    Protected Function HexArrayToString(ByRef data As Int16()) As String
        Dim output As New StringBuilder()
        For Each value As Int16 In data
            output.Append(Conversion.Hex(value) & ",")
        Next
        Return output.ToString
    End Function

    Protected Function CalculateCheckSum(ByRef command As COMMAND, ByRef tag As Byte, ByRef values As Int16()) As Byte
        Dim sum As Int16
        Try
            sum += command + tag
            For Each value As Int16 In values
                ' gencg only reads the positive value and ignore negative values (but does read signed int16)
                If value >= 0 Then
                    sum += value
                End If
            Next
        Catch ex As OverflowException
            sum = Int16.MaxValue
        End Try

        Dim byteSum As Byte() = BitConverter.GetBytes(sum)

        ' GenCG only requires the lower byte of the sum. So we will sum the values and then return the last byte
        ' the order of bytes returned depends on the cpu architecture, so we will check to ensure the correct byte is sent
        If BitConverter.IsLittleEndian Then
            Return byteSum(0)
        Else
            Return byteSum(1)
        End If
    End Function

    Protected Function SecondsToTenths(ByVal seconds As Double) As Int16
        Return Convert.ToInt16(seconds * 100)
    End Function

    Public Function ReadMessage() As Byte()
        If Not Connected() Or Not netStream.CanRead Then
            Return Nothing
        End If

        Dim inStream(netClient.ReceiveBufferSize) As Byte

        Try
            netStream.Read(inStream, 0, inStream.Length)
            Debug.WriteLine("Response from GenCG: " & String.Join(",", inStream))
        Catch ex As SocketException
            Debug.WriteLine("Error " & ex.ErrorCode & " - " & ex.InnerException.Message)
        Catch ex As Exception
            Debug.WriteLine("Response from GenCG: Error Timed Out " & ex.Message)
        End Try

        Return inStream
    End Function

    Public Function SendMessage(ByRef data As Byte()) As Boolean
        If Not Connected Or Not netStream.CanWrite Then
            Return False
        End If

        Debug.WriteLine("Message to GenCG: " & String.Join(",", data))

        Try
            netStream.Write(data, 0, data.Length)
        Catch ex As SocketException
            Debug.WriteLine("Error " & ex.ErrorCode & " - " & ex.InnerException.Message)
        Catch ex As Exception
            Debug.WriteLine("Message to GenCG: Error Timed Out")
            Return False
        End Try

        Return True
    End Function

    Public Function SendCommand(ByRef command As COMMAND, ByRef tag As Byte, ParamArray data As Int16()) As RESPONSE
        Return ParseResponseStatus(SendRawCommand(command, tag, data))
    End Function

    Public Function SendRawCommand(ByRef command As COMMAND, ByRef tag As Byte, ParamArray data As Int16()) As Byte()
        Debug.WriteLine("User Params: " & HexArrayToString(data))

        Dim response As Byte() = {}

        Dim sendData As Byte() = PrepareData(command, tag, data)

        If SendMessage(sendData) Then
            response = ReadMessage()
        End If

        StandBy()

        Return response
    End Function

    Private Function PrepareData(ByRef command As COMMAND, ByRef tag As Byte, ParamArray data As Int16()) As Byte()
        ' command, tag, params (n*2), checksum (size is defined as 0 index)
        Dim sendData(2 + (data.Length * 2)) As Byte
        sendData(0) = command
        sendData(1) = tag

        For i = 0 To data.Length - 1
            Dim offset As Integer = (i * 2) + 2

            ' we offset by 2 and 3 because the first two elements are the command and tag
            If data(i) >= 0 Then
                ' we jump by two since we are setting two elements per iteration

                ' the order of bytes returned depends on the cpu architecture, so we will check to ensure the correct byte is sent
                If BitConverter.IsLittleEndian Then
                    sendData(offset) = BitConverter.GetBytes(data(i))(0)
                    sendData(offset + 1) = BitConverter.GetBytes(data(i))(1)
                Else
                    sendData(offset) = BitConverter.GetBytes(data(i))(1)
                    sendData(offset + 1) = BitConverter.GetBytes(data(i))(0)
                End If

            Else
                sendData(offset) = 0
                sendData(offset + 1) = 0
            End If
        Next

        sendData(sendData.Length - 1) = CalculateCheckSum(command, tag, data)

        Return sendData
    End Function

    Public Function ParseResponseStatus(ByRef responseData As Byte()) As RESPONSE
        If responseData.Length <= 0 Then
            Return RESPONSE.SEND_ERROR
        End If

        Select Case responseData(0)
            Case RESPONSE.BAD_COMMAND, RESPONSE.BAD_PARAMETER, RESPONSE.CANNOT_PROCESS, RESPONSE.FINISH_PLAYBACK
                Return CType(responseData(0), RESPONSE)
            Case Else
                ' if the response is not any of those defined messages then we will assume gencg
                ' has responded back with the command we sent. if so, this means the call was good!
                Return RESPONSE.SUCCESS
        End Select
    End Function

#Region "GenCG TCP Calls"

    Public Sub StandBy()
        SendMessage(Enumerable.Repeat(Of Byte)(COMMAND.SEPARATOR, 4).ToArray())
    End Sub

    Public Function Cut(Optional ByVal start As Int16 = 1, Optional ByVal delaySeconds As Double = 0, Optional ByVal tag As Byte = 0) As RESPONSE
        Return SendCommand(COMMAND.CUT, tag, start, SecondsToTenths(delaySeconds))
    End Function

    Public Function Clear(Optional ByVal tag As Byte = 0) As RESPONSE
        Return SendCommand(COMMAND.CLEAR, tag)
    End Function

    Public Function TakeIn(Optional ByVal startPage As Int16 = 1, Optional ByVal delaySeconds As Double = 0, Optional ByVal tag As Byte = 0) As RESPONSE
        Return SendCommand(COMMAND.TAKE_IN, tag, startPage, SecondsToTenths(delaySeconds))
    End Function

    Public Function TakeOut(Optional ByVal tag As Byte = 0) As RESPONSE
        Return SendCommand(COMMAND.TAKE_OUT, tag)
    End Function

    Public Function StopSequence(Optional ByVal tag As Byte = 0) As RESPONSE
        Return SendCommand(COMMAND.STOP_SEQUENCE, tag)
    End Function

    Public Function StartSequence(Optional ByVal startPage As Int16 = 0, Optional ByVal endPage As Int16 = 0,
                                  Optional ByVal loopCount As Int16 = 1, Optional ByVal loopDelaySeconds As Double = 0,
                                  Optional ByVal tag As Byte = 0) As RESPONSE

        Return SendCommand(COMMAND.START_SEQUENCE, tag, startPage, endPage, loopCount, SecondsToTenths(loopDelaySeconds))
    End Function

    Public Function LoadPage(Optional ByVal page As Byte = 1, Optional ByVal tag As Byte = 0) As RESPONSE
        Return SendCommand(COMMAND.LOAD_PAGE, tag, page)
    End Function

    Public Function GetDocumentInfo(Optional ByVal tag As Byte = 0) As DocumentInfo?
        Dim response As Byte() = SendRawCommand(COMMAND.DOCUMENT_INFO, tag)

        If response(0) <> COMMAND.DOCUMENT_INFO Then
            Return Nothing
        End If

        ' the first two bytes gencg sends back is the command and tag
        Return New DocumentInfo With {
            .TotalPages = BitConverter.ToInt16(response, 2),
            .StartPage = BitConverter.ToInt16(response, 4),
            .EndPage = BitConverter.ToInt16(response, 6)
        }
    End Function

#End Region

End Class
