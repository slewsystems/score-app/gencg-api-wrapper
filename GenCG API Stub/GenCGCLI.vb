﻿Imports com.slewsystems.gencg.api
Imports CommandLine

Public Interface IOptions
    <CommandLine.Option("v"c, DefaultValue:=False, HelpText:="Verbose output")>
    Property Verbose As Boolean

    Function DoWork() As Integer

End Interface

Public MustInherit Class TcpOptions

    Public Property GenCGTCP As New GenCGTCP

End Class

Public MustInherit Class AppOptions

    Public Property GenCGApplication As New GenCGApplication

End Class

Public Class Options
    Implements IOptions

    <VerbOption("cut", HelpText:="Cut a page")>
    Public Property Cut As New CutOptions

    <VerbOption("clear", HelpText:="Clear page")>
    Public Property Clear As New ClearOptions

    <VerbOption("take-in", HelpText:="Take in page")>
    Public Property TakeIn As New TakeInOptions

    <VerbOption("take-out", HelpText:="Take out page")>
    Public Property TakeOut As New TakeOutOptions

    <VerbOption("start", HelpText:="Start sequence")>
    Public Property Start As New StartOptions

    <VerbOption("stop", HelpText:="Stop sequence")>
    Public Property [Stop] As New StopOptions

    <VerbOption("load", HelpText:="Load page")>
    Public Property Load As New LoadOptions

    <VerbOption("doc-info", HelpText:="Document information")>
    Public Property Info As New InfoOptions

    <VerbOption("reset", HelpText:="Reset GenCG api state")>
    Public Property Reset As New ResetOptions

    <VerbOption("i", HelpText:="Interpretter")>
    Public Property Interpretter As New InterpretterOptions

    <VerbOption("app-info", HelpText:="GenCG information")>
    Public Property AppInfo As New AppInfoOptions

    <VerbOption("workspace-info", HelpText:="GenCG workspace information")>
    Public Property WorkspaceInfo As New WorkspaceInfoOptions

    <VerbOption("activate", HelpText:="Activate a document")>
    Public Property SwitchDoc As New SwitchDocumentOptions

    <VerbOption("launch", HelpText:="Start GenCG")>
    Public Property LaunchApp As New LaunchOptions

    Public Property Verbose As Boolean Implements IOptions.Verbose

    <HelpVerbOption("help", HelpText:="Show this help text")>
    Public Function GetUsage(ByVal verbName As String) As String
        Dim help As CommandLine.Text.HelpText = CommandLine.Text.HelpText.AutoBuild(Me, verbName)

        With help
            .Heading = My.Application.Info.Title
            .Copyright = My.Application.Info.Copyright
            .AddDashesToOption = True
            .AdditionalNewLineAfterOption = False
        End With

        Return help
    End Function

    Public Function DoWork() As Integer Implements IOptions.DoWork
        Return Nothing
    End Function

End Class

#Region "TCP Options"

Public Class CutOptions
    Inherits TcpOptions
    Implements IOptions

    <CommandLine.Option("p"c, "page", DefaultValue:=1, HelpText:="Page number", Required:=True)>
    Public Property Page As Int32

    <CommandLine.Option("d"c, "delay", DefaultValue:=0, HelpText:="Delay in seconds")>
    Public Property Delay As Double

    Public Property Verbose As Boolean Implements IOptions.Verbose

    Public Function DoWork() As Integer Implements IOptions.DoWork
        If GenCGTCP.Connect() Then
            GenCGTCP.Cut(Page, Delay)
            GenCGTCP.Disconnect()

            Return 0
        End If

        Return 1
    End Function

End Class

Public Class TakeInOptions
    Inherits TcpOptions
    Implements IOptions

    <CommandLine.Option("p"c, "page", DefaultValue:=1, HelpText:="Page number", Required:=True)>
    Public Property Page As Int32

    <CommandLine.Option("d"c, "delay", DefaultValue:=0, HelpText:="Delay in seconds")>
    Public Property Delay As Double

    Public Property Verbose As Boolean Implements IOptions.Verbose

    Public Function DoWork() As Integer Implements IOptions.DoWork
        If GenCGTCP.Connect() Then
            GenCGTCP.TakeIn(Page, Delay)
            GenCGTCP.Disconnect()

            Return 0
        End If


        Return 1
    End Function

End Class

Public Class TakeOutOptions
    Inherits TcpOptions
    Implements IOptions

    Public Property Verbose As Boolean Implements IOptions.Verbose

    Public Function DoWork() As Integer Implements IOptions.DoWork
        If GenCGTCP.Connect() Then
            GenCGTCP.TakeOut()
            GenCGTCP.Disconnect()

            Return 0
        End If

        Return 1
    End Function
End Class

Public Class ClearOptions
    Inherits TcpOptions
    Implements IOptions

    Public Property Verbose As Boolean Implements IOptions.Verbose

    Public Function DoWork() As Integer Implements IOptions.DoWork
        If GenCGTCP.Connect() Then
            GenCGTCP.Clear()
            GenCGTCP.Disconnect()

            Return 0
        End If

        Return 1
    End Function
End Class

Public Class StartOptions
    Inherits TcpOptions
    Implements IOptions

    <CommandLine.OptionList("p"c, "page", DefaultValue:=New Int32() {0}, HelpText:="Start and end page")>
    Public Property Pages As Int32()

    <CommandLine.Option("d"c, "delay", DefaultValue:=0, HelpText:="Delay in seconds")>
    Public Property Delay As Double

    <CommandLine.Option("c"c, "count", DefaultValue:=1, HelpText:="Loop iterations")>
    Public Property LoopCount As Int32

    Public Property Verbose As Boolean Implements IOptions.Verbose

    Public Function DoWork() As Integer Implements IOptions.DoWork
        If GenCGTCP.Connect() Then
            If Pages.Length > 1 Then
                GenCGTCP.StartSequence(Pages(0), Pages(1), LoopCount, Delay)
            Else
                GenCGTCP.StartSequence(Pages(0),, LoopCount, Delay)
            End If

            GenCGTCP.Disconnect()

            Return 0
        End If

        Return 1
    End Function
End Class

Public Class StopOptions
    Inherits TcpOptions
    Implements IOptions

    Public Property Verbose As Boolean Implements IOptions.Verbose

    Public Function DoWork() As Integer Implements IOptions.DoWork
        If GenCGTCP.Connect() Then
            GenCGTCP.StopSequence()
            GenCGTCP.Disconnect()

            Return 0
        End If

        Return 1
    End Function
End Class

Public Class LoadOptions
    Inherits TcpOptions
    Implements IOptions

    <CommandLine.Option("p"c, "page", DefaultValue:=1, HelpText:="Page number", Required:=True)>
    Public Property Page As Int32

    Public Property Verbose As Boolean Implements IOptions.Verbose

    Public Function DoWork() As Integer Implements IOptions.DoWork
        If GenCGTCP.Connect() Then
            GenCGTCP.LoadPage(Page)
            GenCGTCP.Disconnect()

            Return 0
        End If

        Return 1
    End Function

End Class

Public Class InfoOptions
    Inherits TcpOptions
    Implements IOptions

    Public Property Verbose As Boolean Implements IOptions.Verbose

    Public Function DoWork() As Integer Implements IOptions.DoWork
        If GenCGTCP.Connect() Then
            Dim doc As GenCGTCP.DocumentInfo? = GenCGTCP.GetDocumentInfo()
            If doc.HasValue Then
                Console.WriteLine("Pages" & ControlChars.Tab & doc.Value.TotalPages.ToString)
                Console.WriteLine("Start" & ControlChars.Tab & doc.Value.StartPage.ToString)
                Console.WriteLine("End" & ControlChars.Tab & doc.Value.EndPage.ToString)
            End If
            GenCGTCP.Disconnect()

            Return 0
        End If

        Return 1
    End Function
End Class

Public Class InterpretterOptions
    Implements IOptions

    Public Property Verbose As Boolean Implements IOptions.Verbose

    Public Function DoWork() As Integer Implements IOptions.DoWork
        Return Nothing
    End Function

End Class

Public Class ResetOptions
    Inherits TcpOptions
    Implements IOptions

    Public Property Verbose As Boolean Implements IOptions.Verbose

    Public Function DoWork() As Integer Implements IOptions.DoWork
        If GenCGTCP.Connect() Then
            GenCGTCP.StandBy()
            GenCGTCP.Disconnect()

            Return 0
        End If

        Return 1
    End Function
End Class

#End Region

#Region "App Options"

Public Class AppInfoOptions
    Inherits AppOptions
    Implements IOptions

    Public Property Verbose As Boolean Implements IOptions.Verbose

    Public Function DoWork() As Integer Implements IOptions.DoWork
        GenCGApplication.Update()

        Console.WriteLine(GenCGApplication.Info.ToString())

        Return Nothing
    End Function
End Class

Public Class WorkspaceInfoOptions
    Inherits AppOptions
    Implements IOptions

    Public Property Verbose As Boolean Implements IOptions.Verbose

    Public Function DoWork() As Integer Implements IOptions.DoWork
        GenCGApplication.Update()

        Dim documents As GenCGDocumentList = GenCGApplication.GetOpenedDocuments()
        For Each doc As GenCGDocument In documents
            Console.WriteLine(doc.ToString)
        Next

        Return Nothing
    End Function
End Class

Public Class SwitchDocumentOptions
    Inherits AppOptions
    Implements IOptions

    <CommandLine.Option("t"c, "tab", DefaultValue:=-1, HelpText:="Tab index", MutuallyExclusiveSet:="index")>
    Public Property TabIndex As Int32

    <CommandLine.Option("d"c, "document", HelpText:="Document name", MutuallyExclusiveSet:="name")>
    Public Property DocumentName As String

    <CommandLine.Option("f"c, "file", HelpText:="Path to file to open", MutuallyExclusiveSet:="file")>
    Public Property FileOpen As String

    Public Property Verbose As Boolean Implements IOptions.Verbose

    Public Function DoWork() As Integer Implements IOptions.DoWork
        GenCGApplication.Update()

        Dim docs = GenCGApplication.GetOpenedDocuments()
        Dim targetDocument As GenCGDocument = Nothing

        If DocumentName <> String.Empty Then
            targetDocument = docs.FindByTitle(DocumentName)
        ElseIf TabIndex >= 0 Then
            targetDocument = docs.Item(TabIndex)
        ElseIf FileOpen <> String.Empty Then
            GenCGApplication.OpenFile(FileOpen)
        Else
            GenCGApplication.CycleActiveDocument()
        End If

        If targetDocument Is Nothing And FileOpen = String.Empty Then
            Console.WriteLine("No document found")
            Return 1
        End If

        If targetDocument IsNot Nothing Then
            targetDocument.Activate()
        End If

        Return Nothing
    End Function
End Class

Public Class LaunchOptions
    Inherits AppOptions
    Implements IOptions

    Public Property Verbose As Boolean Implements IOptions.Verbose

    <CommandLine.Option("n"c, "new", DefaultValue:=False, HelpText:="Open new app instance if already running", MutuallyExclusiveSet:="new")>
    Public Property NewInstance As Boolean

    <CommandLine.Option("f"c, "close", DefaultValue:=False, HelpText:="Close any existing GenCG instances and open a new one", MutuallyExclusiveSet:="existing")>
    Public Property CloseExisting As Boolean

    Public Function DoWork() As Integer Implements IOptions.DoWork
        GenCGApplication.Update()

        If NewInstance Or Not GenCGApplication.Running Then
            GenCGApplication.Launch()
        Else
            If CloseExisting Then
                GenCGApplication.Close()
                GenCGApplication.Launch()
            Else
                GenCGApplication.BringToFront()
            End If
        End If


        Return Nothing
    End Function
End Class

#End Region

Module GenCGCLI

    Private Function ProcessCommand(verbName As String, verbOptions As IOptions) As Integer
        If verbOptions Is Nothing Then
            Return 1
        End If

        If TypeOf verbOptions Is InterpretterOptions Then
            Console.WriteLine("Interpretter mode, enter a command:")
            Do
                Console.Write("> ")
                Dim commandString As String = Console.ReadLine()
                Dim sArgs() As String = commandString.Split(" "c)

                If sArgs(0).ToLower().Equals("exit") Then
                    Return 0
                End If

                ' pretty much, don't show the error if passing nothing
                If Not sArgs(0).Equals(String.Empty) Then
                    Main(sArgs)
                End If
            Loop
        Else
            Return verbOptions.DoWork()
        End If

        Return 0
    End Function

    Private Sub ParseError()

    End Sub

    Sub Main(ByVal sArgs() As String)
        Dim options As New Options
        Parser.Default.ParseArgumentsStrict(sArgs, options, AddressOf ProcessCommand, AddressOf ParseError)
    End Sub

End Module
