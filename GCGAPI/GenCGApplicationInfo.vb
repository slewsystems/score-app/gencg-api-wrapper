﻿Imports System.IO

Public Class GenCGApplicationInfo

    Public Enum GenCGVersion As Integer
        UNKNOWN = -1
        GCG_4_5 = 45
        GCG_4_6 = 46
        GCG_4_7 = 47
        GCG_5_0 = 50
        GCG_5_5 = 55
    End Enum

    Public Property PID As Integer
    Public Property WindowHandle As IntPtr
    Public Property MDIHandle As IntPtr
    Public Property Version As GenCGVersion
    Public Property Path As String

    Private Shared SearchDirectories() As DirectoryInfo = {
        New DirectoryInfo("C:\Program Files (x86)\Compix\GenCG"),
        New DirectoryInfo("C:\Program Files\Compix\GenCG")
    }

    Sub New()
        Reset()
    End Sub

    Public Sub Reset()
        PID = -1
        WindowHandle = IntPtr.Zero
        MDIHandle = IntPtr.Zero
        Version = GenCGVersion.UNKNOWN
        Path = Nothing
    End Sub

    Private Function GetMainWindowHandle() As IntPtr
        Return FindWindowHandle("gencg")
    End Function

    Public Sub Update()
        WindowHandle = GetMainWindowHandle()
        MDIHandle = FindMDIHandle(WindowHandle)
        PID = GetPIDFromHandle(WindowHandle)
        Path = GetPathToProcessExecutable(PID)
        Version = GetVersion(Path)
    End Sub

    Public Shared Function GetApplicationPath() As String
        For Each ProgramDirectory As DirectoryInfo In SearchDirectories
            Dim results() As FileInfo = ProgramDirectory.GetFiles("*gencg*.exe")
            If results.Length > 0 Then
                Return results(0).FullName()
            End If
        Next

        Return String.Empty
    End Function

    Public Shared Function FindMDIHandle(mainWindow As IntPtr) As IntPtr
        If mainWindow <> IntPtr.Zero Then
            Return FindWindowEx(mainWindow, IntPtr.Zero, Nothing, Nothing) 'returns first handle inside handle var
        End If
        Return IntPtr.Zero
    End Function

    Public Shared Function GetVersion(executableFilePath As String) As GenCGVersion
        If executableFilePath <> String.Empty Then
            Dim ver() As String = FileVersionInfo.GetVersionInfo(executableFilePath).FileVersion.Split(","c)
            Dim major As Integer = Integer.Parse(ver(0))
            Dim minor As Integer = Integer.Parse(ver(1))
            If major = 5 Then
                If minor = 0 Then
                    Return GenCGVersion.GCG_5_0
                ElseIf minor = 5 Then
                    Return GenCGVersion.GCG_5_5
                End If
            ElseIf major = 4 Then
                If minor = 5 Then
                    Return GenCGVersion.GCG_4_5
                ElseIf minor = 6 Then
                    Return GenCGVersion.GCG_4_6
                ElseIf minor = 7 Then
                    Return GenCGVersion.GCG_4_7
                End If
            End If
        End If

        Return GenCGVersion.UNKNOWN
    End Function

    Public Overrides Function ToString() As String
        Return "Title" & ControlChars.Tab & GetWindowTitleFromHandle(WindowHandle) & vbCrLf &
            "Handle" & ControlChars.Tab & WindowHandle.ToString & vbCrLf &
            "PID" & ControlChars.Tab & PID.ToString & vbCrLf &
            "Path" & ControlChars.Tab & Path & vbCrLf &
            "Version" & ControlChars.Tab & Version.ToString
    End Function

End Class
