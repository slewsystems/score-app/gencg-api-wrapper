﻿Public Class GenCGDocument

    Public mdicontainter As IntPtr
    Public hwnd As IntPtr

    Sub New(handle As IntPtr, container As IntPtr)
        hwnd = handle
        mdicontainter = container
    End Sub

    Public ReadOnly Property Title As String
        Get
            ' TODO: change logic not to turn entire filename to lower case (maybe split by . and remove last element if its gcg)
            Return GetWindowTitleFromHandle(hwnd).ToLower.Replace(".gcg", "")
        End Get
    End Property

    Public ReadOnly Property Alive As Boolean
        Get
            Return Not (hwnd = IntPtr.Zero Or mdicontainter = IntPtr.Zero)
        End Get
    End Property

    Public Sub Maximize()
        MaximizeHWnd(hwnd)
    End Sub

    Public Sub Close()
        CloseHWnd(hwnd)
    End Sub

    Public Sub Activate()
        PostMessage(mdicontainter, WINDOWS_MESSAGES.WM_MDIACTIVATE, hwnd, IntPtr.Zero)
    End Sub

    Public Overrides Function ToString() As String
        Return Title & ControlChars.Tab & hwnd.ToString
    End Function

    Public Overrides Function Equals(obj As Object) As Boolean
        Return Me.hwnd.Equals(obj)
    End Function

End Class
