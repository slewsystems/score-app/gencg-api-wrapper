﻿Public Class GenCGDocumentList
    Inherits List(Of GenCGDocument)

    Public Function GetActiveDocument() As GenCGDocument
        Return Me.Item(0) ' we always get the active document as the first item
    End Function

    Public Function FindByTitle(title As String) As GenCGDocument
        For Each doc As GenCGDocument In Me
            If doc.Title.ToLower().Equals(title.ToLower()) Then
                Return doc
            End If
        Next
        Return Nothing
    End Function

End Class